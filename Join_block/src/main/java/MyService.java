
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.RowExtractor;

public class MyService implements WorkflowBlock {
    /**
     * This field refers to property 'Copy Input' defined in service.xml
     */
    private final static String Join_condition = "Join";

    /**
     * This field refers to input port 'input-1' defined in service.xml
     */
    private final static String Input_INPUT_1 = "input_1";
    private final static String Input_INPUT_2 = "input_2";
    /**
     * This field refers to output port 'output-1' defined in service.xml
     */
    private final static String Output_OUTPUT_1 = "output-1";


    /**
     * This method is called when block execution is first started. It should be
     * used to setup any data structures that are used throughout the execution
     * lifetime of the block.
     */
    public void preExecute(BlockEnvironment env) throws Exception
    {
        
    }

    /**
     * This code is used to perform the actual block operation. It may be called
     * multiple times if data is being streamed through the block. It is, however, 
     * guaranteed to be called at least once and always after the preExecute
     * method and always before the postExecute method;
     */
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception
    {
            Data outputData=new Data();
            Data input_Data1 = inputs.getInputDataSet(Input_INPUT_1);
            Data input_Data2 = inputs.getInputDataSet(Input_INPUT_2);
            String condition=env.getStringProperty(Join_condition, "");
            Map<String,String> dataset = new IdentityHashMap<String ,String>();  
            Binding context=new Binding();
    		Pattern input_1Pattern = Pattern.compile("input_1");
    		Matcher input_1Matcher = input_1Pattern.matcher(condition);
    		Pattern input_2Pattern = Pattern.compile("input_2");
    		Matcher input_2Matcher = input_2Pattern.matcher(condition);
    		int index = 0;
    		String input1 = null;
    		String input2 = null;
    		Pattern Pattern_1 = Pattern.compile("input_1.\\{.*?\\}");
    		Matcher Matcher_1 = Pattern_1.matcher(condition);
    		Pattern Pattern_2 = Pattern.compile("input_2.\\{.*?\\}");
    		Matcher Matcher_2 = Pattern_2.matcher(condition);		
    		condition = condition.replace("{", "").replace("}", "");			
    		while (Matcher_1.find()&&input_1Matcher.find()) {
    			String col = Matcher_1.group().substring(Matcher_1.group().indexOf("{")+1, Matcher_1.group().indexOf("}"));	
    			System.out.println(input_1Matcher.group());
    			input1 = new String(input_1Matcher.group());
    			dataset.put(input1, col);
    			Matcher matcher = Pattern.compile(col).matcher(condition);
    			String tempinput = "column(column" + index
    					+ ").getObjectValue(row1)";
    			context.setVariable("column" + index, col);
    			condition = matcher.replaceFirst(tempinput);
    			index++;
    		}			
    		while (Matcher_2.find()&&input_2Matcher.find()) {
    			String col = Matcher_2.group().substring(Matcher_2.group().indexOf("{")+1, Matcher_2.group().indexOf("}"));
    			input2 = new String(input_2Matcher.group());
    			dataset.put(input2, col);
    			Matcher matcher = Pattern.compile(col).matcher(condition);
    			String tempinput = "column(column" + index
    					+ ").getObjectValue(row2)";
    			context.setVariable("column" + index, col);
    			condition = matcher.replaceFirst(tempinput);
    			index++;
    		}
    		System.out.println(condition);
    		RowExtractor extractor_1 = new RowExtractor(input_Data1);
    		RowExtractor extractor_2 = new RowExtractor(input_Data2);
    		List<Integer> Selectedindex_1 = new ArrayList<>();
    		List<Integer> Selectedindex_2 = new ArrayList<>();
    		context.setVariable("input_1", input_Data1);
    		context.setVariable("input_2", input_Data2);
    		for (int row1 = 0; row1 < input_Data1.getLargestRows(); row1++) {
    			for (int row2 = 0; row2 < input_Data2.getLargestRows(); row2++) {
    				
    				context.setVariable("row1", row1);
    				context.setVariable("row2", row2);
    				GroovyShell shell = new GroovyShell(context);
    				boolean Boolean = (boolean) shell.evaluate(condition);
    				if (Boolean) {
    					System.out.println("row1: " + row1);
    					Selectedindex_1.add(row1);
    					System.out.println("row2: " + row2);
    					Selectedindex_2.add(row2);
    				}
    			}
    		}
    		Integer[] indexs_1 = Selectedindex_1.toArray(new Integer[0]);
    		Integer[] indexs_2 = Selectedindex_2.toArray(new Integer[0]);
    		outputData = extractor_1.extract(indexs_1);
    		outputData.joinData(extractor_2.extract(indexs_2), true);
    	
        // Pass this to the output called "output-1"
        outputs.setOutputDataSet(Output_OUTPUT_1, outputData);
    }
    
    /*
     * This code is called once when all of the data has passed through the block. 
     * It should be used to cleanup any resources that the block has made use of.
     */
    public void postExecute(BlockEnvironment env) throws Exception
    {
        
    }
}
