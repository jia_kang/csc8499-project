
import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.manipulation.RowExtractor;
/*
 * Author：KANG JIA
 * Description：Use groovy to achieve the join function
 * */
public class example {
	
	public static Binding context=new Binding();
	public static Map<String,String> dataset = new IdentityHashMap<String ,String>();  
	public Data generateData1() throws IndexOutOfBoundsException, DataException {
		Data input = new Data();
		IntegerColumn Age = new IntegerColumn("Age");
		StringColumn Sex = new StringColumn("Sex");
		IntegerColumn ID = new IntegerColumn("ID");
		DoubleColumn Score= new DoubleColumn("Score");
		ID.nullifyToSize(10);
		Age.nullifyToSize(10);
		Score.nullifyToSize(10);
		Score.setDoubleValue(0, 60.7);
		Score.setDoubleValue(1, 66.8);
		Score.setDoubleValue(2, 73.8);
		Score.setDoubleValue(3, 74.6);
		Score.setDoubleValue(4, 80.88);
		Score.setDoubleValue(5, 81.82);
		Score.setDoubleValue(6, 84.74);
		Score.setDoubleValue(7, 90.6);
		Score.setDoubleValue(8, 97.77);
		Score.setDoubleValue(9, 100);
		ID.setIntValue(0, 1);
		ID.setIntValue(1,2);
		ID.setIntValue(2,3);
		ID.setIntValue(3, 4);
		ID.setIntValue(4, 5);
		ID.setIntValue(5, 6);
		ID.setIntValue(6, 7);
		ID.setIntValue(7, 8);
		ID.setIntValue(8, 9);
		ID.setIntValue(9, 10);
		Age.setIntValue(0, 1);
		Age.setIntValue(1, 34);
		Age.setIntValue(2, 12);
		Age.setIntValue(3, 54);
		Age.setIntValue(4, 26);
		Age.setIntValue(5, 31);
		Age.setIntValue(6, 20);
		Age.setIntValue(7, 27);
		Age.setIntValue(8, 30);
		Age.setIntValue(9, 30);
		Sex.insertObjectValue(0, "Male", true);
		Sex.insertObjectValue(1, "Female", true);
		Sex.insertObjectValue(2, "Female", true);
		Sex.insertObjectValue(3, "Male", true);
		Sex.insertObjectValue(4, "Male", true);
		Sex.insertObjectValue(5, "Female", true);
		Sex.insertObjectValue(6, "Male", true);
		Sex.insertObjectValue(7, "Female", true);
		Sex.insertObjectValue(8, "Male", true);
		Sex.insertObjectValue(9, "Female", true);
		input.addColumn(ID);
		input.addColumn(Sex);
		input.addColumn(Age);
		input.addColumn(Score);
		return input;
	}

	public Data generateData2() throws IndexOutOfBoundsException, DataException {
		Data input_Data2 = new Data();
		IntegerColumn ID = new IntegerColumn("ID");
		ID.nullifyToSize(10);
		Column Country = new StringColumn("Country");
		Country.insertObjectValue(0, "China", true);
		Country.insertObjectValue(1, "Australia", true);
		Country.insertObjectValue(2, "Africa", true);
		Country.insertObjectValue(3, "Korea", true);
		Country.insertObjectValue(4, "USA", true);
		Country.insertObjectValue(5, "UK", true);
		Country.insertObjectValue(6, "Female", true);
		Country.insertObjectValue(7, "9", true);
		Country.insertObjectValue(8, "9", true);
		Country.insertObjectValue(9, "9", true);
		ID.setIntValue(0, 9);
		ID.setIntValue(1, 1);
		ID.setIntValue(2, 3);
		ID.setIntValue(3, 4);
		ID.setIntValue(4, 5);
		ID.setIntValue(5, 6);
		ID.setIntValue(6, 7);
		ID.setIntValue(7, 8);
		ID.setIntValue(8, 9);
		ID.setIntValue(9, 10);

		input_Data2.addColumn(ID);
		input_Data2.addColumn(Country);
		return input_Data2;
	}
	
	public String generateGroovyInput(String condition)
 throws IndexOutOfBoundsException, DataException {
		Pattern input_1Pattern = Pattern.compile("input_1");
		Matcher input_1Matcher = input_1Pattern.matcher(condition);
		Pattern input_2Pattern = Pattern.compile("input_2");
		Matcher input_2Matcher = input_2Pattern.matcher(condition);
		int index = 0;
		String input1 = null;
		String input2 = null;
		Pattern Pattern_1 = Pattern.compile("input_1.\\{.*?\\}");
		Matcher Matcher_1 = Pattern_1.matcher(condition);
		Pattern Pattern_2 = Pattern.compile("input_2.\\{.*?\\}");
		Matcher Matcher_2 = Pattern_2.matcher(condition);
		
		condition = condition.replace("{", "").replace("}", "");
		
		
			
		while (Matcher_1.find()&&input_1Matcher.find()) {

			String col = Matcher_1.group().substring(Matcher_1.group().indexOf("{")+1, Matcher_1.group().indexOf("}"));
		
			System.out.println(input_1Matcher.group());
			input1 = new String(input_1Matcher.group());
			dataset.put(input1, col);
			Matcher matcher = Pattern.compile(col).matcher(condition);
			String tempinput = "column(column" + index
					+ ").getObjectValue(row1)";
			context.setVariable("column" + index, col);
			condition = matcher.replaceFirst(tempinput);
			index++;
		}
		
	
			
		while (Matcher_2.find()&&input_2Matcher.find()) {
			
			String col = Matcher_2.group().substring(Matcher_2.group().indexOf("{")+1, Matcher_2.group().indexOf("}"));
			input2 = new String(input_2Matcher.group());
			dataset.put(input2, col);
			Matcher matcher = Pattern.compile(col).matcher(condition);
			String tempinput = "column(column" + index
					+ ").getObjectValue(row2)";
			context.setVariable("column" + index, col);
			condition = matcher.replaceFirst(tempinput);
			index++;
		}
	
		System.out.println(condition);
		return condition;
	}
	
	public Data generateOutput(Data input_Data1, Data input_Data2,
			String condition) throws IndexOutOfBoundsException, DataException {
		Data output = new Data();
		RowExtractor extractor_1 = new RowExtractor(input_Data1);
		RowExtractor extractor_2 = new RowExtractor(input_Data2);
		List<Integer> Selectedindex_1 = new ArrayList<>();
		List<Integer> Selectedindex_2 = new ArrayList<>();
		context.setVariable("input_1", input_Data1);
		context.setVariable("input_2", input_Data2);
		for (int row1 = 0; row1 < input_Data1.getLargestRows(); row1++) {
			for (int row2 = 0; row2 < input_Data2.getLargestRows(); row2++) {
				context.setVariable("row1", row1);
				context.setVariable("row2", row2);
				GroovyShell shell = new GroovyShell(context);
				boolean Boolean = (boolean) shell.evaluate(condition);
				if (Boolean) {
					System.out.println("row1: " + row1);
					Selectedindex_1.add(row1);
					System.out.println("row2: " + row2);
					Selectedindex_2.add(row2);
				}
			}
		}
		Integer[] indexs_1 = Selectedindex_1.toArray(new Integer[0]);
		Integer[] indexs_2 = Selectedindex_2.toArray(new Integer[0]);
		output = extractor_1.extract(indexs_1);
		output.joinData(extractor_2.extract(indexs_2), true);
		return output;
	}
	public static void main(String[] arg) throws DataException {
		example example=new example();
		Data output=new Data();
		Data input_Data1=example.generateData1();
		Data input_Data2=example.generateData2();
		//String inputcondition="Math.sqrt(input_1.{Age}-input_2.{ID})==input_2.{ID}";
		String inputcondition="input_1.{ID}==input_2.{ID}/2 && input_1.{Sex}=='Male'";
		System.out.println(inputcondition);
		String condition=example.generateGroovyInput(inputcondition);
		output=example.generateOutput(input_Data1, input_Data2, condition);
		System.out.println("Result:");
		for (int i = 0; i < output.getColumnCount(); i++) {
				System.out.println("column: "+output.column(i).getName());
				System.out.println(output.column(i).getList());
		}		
}
}