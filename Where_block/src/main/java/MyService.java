/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.RowExtractor;

public class MyService implements WorkflowBlock {
	private final static String WHERE_CONDITION = "WHERE";
	private final static String Input_INPUT_1 = "input-1";
	private final static String Output_OUTPUT_1 = "output-1";

	public void preExecute(BlockEnvironment arg0) throws Exception {
	}

	public void execute(BlockEnvironment env, BlockInputs inputs,
			BlockOutputs outputs) throws Exception {
		String condition = env.getStringProperty(WHERE_CONDITION, "");
		System.out.println("condition"+condition);
		Data inputData = inputs.getInputDataSet(Input_INPUT_1);
		List<String> SelectedColumnName=new ArrayList<>();
		final ArrayList<Integer> Index = new ArrayList<Integer>();
		Pattern ColumnPattern = Pattern.compile("\\{.*?\\}");
		Matcher ColumnMatcher = ColumnPattern.matcher(condition);
			while (ColumnMatcher.find()) {
				String ColumnName = ColumnMatcher.group().replace("{", "")
						.replace("}", "");
				System.out.println(ColumnName);
				SelectedColumnName.add(ColumnName);
			} 
			condition = condition.replace("{", "").replace("}", "");
		
			Binding context = new Binding();
			for (int row = 0; row <  inputData.getLargestRows(); row++) {
				//set the value of variable of the condition
				for(String column:SelectedColumnName){
					System.out.println(column);
					context.setVariable(column, inputData.column(column).getObjectValue(row));
				}	
				//implement the condition by GroovyShell and store the index of row which meet the conidtion
				GroovyShell sh = new GroovyShell(context);
				System.out.println(condition);
				boolean Boolean = (boolean) sh.evaluate(condition);
				System.out.println(Boolean);
				if (Boolean) {
					Index.add(row);
				}
		}
			for (Integer xxxxxxxxxx : Index) {
				System.out.println(xxxxxxxxxx);
			}
	       
			Integer[] SelectedIndex = Index.toArray(new Integer[0]);
			RowExtractor extractor=new RowExtractor(inputData);
			Data outputData=extractor.extract(SelectedIndex);
			outputs.setOutputDataSet(Output_OUTPUT_1, outputData);
	}

	public void postExecute(BlockEnvironment env) throws Exception {
	}
}