
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.manipulation.RowExtractor;
import org.pipeline.core.xmlstorage.XmlStorageException;
/*
 * Author：KANG JIA
 * Description：Use groovy to achieve the where function
 * */
public class example {

	public static List<String> SelectedColumnName=new ArrayList<>();
	public Data generate() throws IndexOutOfBoundsException, DataException{
		Data input = new Data();
		IntegerColumn Age = new IntegerColumn("A_ge");
		StringColumn Sex = new StringColumn("Sex");
		IntegerColumn ID = new IntegerColumn("ID");
		DoubleColumn Score= new DoubleColumn("Score");
		ID.nullifyToSize(10);
		Age.nullifyToSize(10);
		Score.nullifyToSize(10);
		Score.setDoubleValue(0, 60.7);
		Score.setDoubleValue(1, 66.8);
		Score.setDoubleValue(2, 73.8);
		Score.setDoubleValue(3, 74.6);
		Score.setDoubleValue(4, 80.88);
		Score.setDoubleValue(5, 81.82);
		Score.setDoubleValue(6, 84.74);
		Score.setDoubleValue(7, 90.6);
		Score.setDoubleValue(8, 97.77);
		Score.setDoubleValue(9, 100);
		ID.setIntValue(0, 1);
		ID.setIntValue(1,2);
		ID.setIntValue(2,3);
		ID.setIntValue(3, 4);
		ID.setIntValue(4, 5);
		ID.setIntValue(5, 6);
		ID.setIntValue(6, 7);
		ID.setIntValue(7, 8);
		ID.setIntValue(8, 9);
		ID.setIntValue(9, 10);
		Age.setIntValue(0, 1);
		Age.setIntValue(1, 34);
		Age.setIntValue(2, 12);
		Age.setIntValue(3, 54);
		Age.setIntValue(4, 26);
		Age.setIntValue(5, 31);
		Age.setIntValue(6, 20);
		Age.setIntValue(7, 27);
		Age.setIntValue(8, 30);
		Age.setIntValue(9, 52);
		Sex.insertObjectValue(0, "Male", true);
		Sex.insertObjectValue(1, "Female", true);
		Sex.insertObjectValue(2, "Female", true);
		Sex.insertObjectValue(3, "Male", true);
		Sex.insertObjectValue(4, "Male", true);
		Sex.insertObjectValue(5, "Fema'le", true);
		Sex.insertObjectValue(6, "Male", true);
		Sex.insertObjectValue(7, "Female", true);
		Sex.insertObjectValue(8, "Male", true);
		Sex.insertObjectValue(9, "Female", true);
		input.addColumn(ID);
		input.addColumn(Sex);
		input.addColumn(Age);
		input.addColumn(Score);
		return input;
	}
	public String generateGroovyInput(Data input,String condition) throws IndexOutOfBoundsException, DataException{	
		Pattern ColumnPattern = Pattern.compile("\\{.*?\\}");
		Matcher ColumnMatcher = ColumnPattern.matcher(condition);
			while (ColumnMatcher.find()) {
				String ColumnName = ColumnMatcher.group().replace("{", "")
						.replace("}", "");
				SelectedColumnName.add(ColumnName);
			} 
			condition = condition.replace("{", "").replace("}", "");
		return condition;
	}
	public Integer[] extractIndex(String condition,Data input) throws IndexOutOfBoundsException, DataException{
		final ArrayList<Integer> Index = new ArrayList<Integer>();
		Binding context = new Binding();
		for (int row = 0; row < input.getLargestRows(); row++) {
			//set the value of variable of the condition
			for(String column:SelectedColumnName){
			
				context.setVariable(column, input.column(column).getObjectValue(row));
				
			}	
			//implement the condition by GroovyShell and store the index of row which meet the conidtion
			GroovyShell sh = new GroovyShell(context);
			boolean Boolean = (boolean) sh.evaluate(condition);
			if (Boolean) {
				Index.add(row);
			}
	}
		for (Integer xxxxxxxxxx : Index) {
			System.out.println(xxxxxxxxxx);
		}
       
		Integer[] SelectedIndex = Index.toArray(new Integer[0]);
		return SelectedIndex;
	}
	public Data generateOutputData(Data input,Integer[] SelectedIndex) throws IndexOutOfBoundsException, DataException{
		RowExtractor extractor=new RowExtractor(input);
		Data output=extractor.extract(SelectedIndex);
		return output;
	}
	public static void main(String[] arg) throws DataException,
			XmlStorageException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	
	String inputcondition ="def id = {ID};{Sex}=='Female' || ({A_ge} * id < 40.6 || {A_ge}/2 > 15 && {Score}> 70.8 || {A_ge} == id);";
		
		example example=new example();
		Data input=example.generate();
		String condition=example.generateGroovyInput(input,inputcondition);
		System.out.println("condition:"+condition);
		//extract the data by index and insert to output data.
		Integer[] SelectedIndex=example.extractIndex(condition,input);

		Data output=example.generateOutputData(input, SelectedIndex);
		System.out.println("Result:");
		for (int i = 0; i < output.getColumnCount(); i++) {
			for (int j = 0; j < output.column(i).getRows(); j++) {
				System.out.println(output.column(i).getName() + ": "
						+ output.column(i).getObjectValue(j));
			}
		}
	}
}
 