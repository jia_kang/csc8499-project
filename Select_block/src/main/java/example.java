import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;


public class example {
	public static List<String> SelectedColumnName=new ArrayList<>();
	public Data generate() throws IndexOutOfBoundsException, DataException {
		Data inputData = new Data();
		IntegerColumn Age = new IntegerColumn("Age");
		StringColumn Sex = new StringColumn("Sex");
		IntegerColumn ID = new IntegerColumn("ID");
		StringColumn country = new StringColumn("Country");
		Age.nullifyToSize(10);
		ID.nullifyToSize(10);
		Age.setIntValue(0, 1);
		Age.setIntValue(1, 34);
		Age.setIntValue(2, 12);
		Age.setIntValue(3, 54);
		Age.setIntValue(4, 26);
		Age.setIntValue(5, 31);
		Age.setIntValue(6, 20);
		Age.setIntValue(7, 27);
		Age.setIntValue(8, 30);
		Age.setIntValue(9, 52);
		Sex.insertObjectValue(0, "Male", true);
		Sex.insertObjectValue(1, "Female", true);
		Sex.insertObjectValue(2, "Female", true);
		Sex.insertObjectValue(3, "Male", true);
		Sex.insertObjectValue(4, "Male", true);
		Sex.insertObjectValue(5, "Female", true);
		Sex.insertObjectValue(6, "Male", true);
		Sex.insertObjectValue(7, "Female", true);
		Sex.insertObjectValue(8, "Male", true);
		Sex.insertObjectValue(9, "Female", true);	
		country.insertObjectValue(0, "china", true);
		country.insertObjectValue(1, "uk", true);
		country.insertObjectValue(2, "usa", true);
		country.insertObjectValue(3, "france", true);
		country.insertObjectValue(4, "italy", true);
		country.insertObjectValue(5, "germany", true);
		country.insertObjectValue(6, "japan", true);
		country.insertObjectValue(7, "korean", true);
		country.insertObjectValue(8, "usa", true);
		country.insertObjectValue(9, "africa", true);
		ID.setIntValue(0, 1);
		ID.setIntValue(1,2);
		ID.setIntValue(2,3);
		ID.setIntValue(3, 4);
		ID.setIntValue(4, 5);
		ID.setIntValue(5, 6);
		ID.setIntValue(6, 7);
		ID.setIntValue(7, 8);
		ID.setIntValue(8, 9);
		ID.setIntValue(9, 10);
		inputData.addColumn(Sex);
		inputData.addColumn(Age);
		inputData.addColumn(ID);
		inputData.addColumn(country);
		return inputData;
	}	

	public String generateGroovyInput(Data input, String condition)
			throws IndexOutOfBoundsException, DataException {
		Pattern ColumnPattern = Pattern.compile("\\{.*?\\}");
		Matcher ColumnMatcher = ColumnPattern.matcher(condition);
		while (ColumnMatcher.find()) {
			String ColumnName = ColumnMatcher.group().replace("{", "")
					.replace("}", "");
			System.out.println(ColumnName);
			SelectedColumnName.add(ColumnName);
		}
		condition = condition.replace("{", "").replace("}", "");
		return condition;
	}

	public Data GenerateOutput(String condition, Data input)
			throws IndexOutOfBoundsException, DataException {
		Data output = new Data();
		Binding context = new Binding();
		for (int row = 0; row < input.getLargestRows(); row++) {
			for (String column : SelectedColumnName) {
				context.setVariable(column, input.column(column)
						.getObjectValue(row));
				System.out.println(column + " :" + context.getVariable(column));
			}
			GroovyShell sh = new GroovyShell(context);
			System.out.println(condition);
			if (sh.evaluate(condition).getClass().equals(ArrayList.class)) {
				@SuppressWarnings("rawtypes")
				ArrayList tempDataset = (ArrayList) sh.evaluate(condition);
				for (int i = 0; i < tempDataset.size(); i++) {
					if (tempDataset.get(i).getClass().equals(String.class)) {
						System.out.println("This is string");

						if (!output.containsColumn("column" + i)) {
							StringColumn newStringcol = new StringColumn(
									"column" + i);
							newStringcol.insertObjectValue(
									newStringcol.getRows(), tempDataset.get(i),
									true);
							output.addColumn(newStringcol);
						} else {
							output.column(i).insertObjectValue(
									output.column(i).getRows(),
									tempDataset.get(i), true);
						}

					} else if (tempDataset.get(i).getClass().equals(Long.class)) {
						System.out.println("This is long");
						if (!output.containsColumn("column" + i)) {
							IntegerColumn newIntegercol = new IntegerColumn(
									"column" + i);
							newIntegercol.insertObjectValue(
									newIntegercol.getRows(),
									tempDataset.get(i), true);

							output.addColumn(newIntegercol);
						} else {
							output.column(i).insertObjectValue(
									output.column(i).getRows(),
									tempDataset.get(i), true);
						}
					} else if (tempDataset.get(i).getClass()
							.equals(Double.class)) {
						System.out.println("This is double");
						if (!output.containsColumn("column" + i)) {
							DoubleColumn newDoublecol = new DoubleColumn(
									"column" + i);
							newDoublecol.insertObjectValue(
									newDoublecol.getRows(), tempDataset.get(i),
									true);

							output.addColumn(newDoublecol);
						} else {
							output.column(i).insertObjectValue(
									output.column(i).getRows(),
									tempDataset.get(i), true);
						}
					}
				}
			} else if (sh.evaluate(condition).getClass()
					.equals(LinkedHashMap.class)) {
				@SuppressWarnings("rawtypes")
				LinkedHashMap tempDataset = (LinkedHashMap) sh
						.evaluate(condition);

				@SuppressWarnings("rawtypes")
				Set key = tempDataset.keySet();
				for (int i = 0; i < key.size(); i++) {

					String columnName = key.toArray()[i].toString();
					System.out.println("tempcol: " + columnName
							+ "  tempresult: " + tempDataset.get(columnName));
					if (tempDataset.get(columnName).getClass()
							.equals(String.class)) {
						System.out.println("This is string");
						if (!output.containsColumn(columnName)) {
							StringColumn newStringcol = new StringColumn(
									columnName);
							newStringcol.insertObjectValue(
									newStringcol.getRows(),
									tempDataset.get(columnName), true);
							output.addColumn(newStringcol);
						} else {
							output.column(columnName).insertObjectValue(
									output.column(columnName).getRows(),
									tempDataset.get(columnName), true);
						}

					} else if (tempDataset.get(columnName).getClass()
							.equals(Long.class)) {
						System.out.println("This is long");
						if (!output.containsColumn(columnName)) {
							IntegerColumn newIntegercol = new IntegerColumn(
									columnName);
							newIntegercol.insertObjectValue(
									newIntegercol.getRows(),
									tempDataset.get(columnName), true);

							output.addColumn(newIntegercol);
						} else {
							output.column(columnName).insertObjectValue(
									output.column(columnName).getRows(),
									tempDataset.get(columnName), true);
						}
					} else if (tempDataset.get(columnName).getClass()
							.equals(Double.class)) {
						System.out.println("This is double");
						if (!output.containsColumn(columnName)) {
							DoubleColumn newDoublecol = new DoubleColumn(
									columnName);
							newDoublecol.insertObjectValue(
									newDoublecol.getRows(),
									tempDataset.get(columnName), true);

							output.addColumn(newDoublecol);
						} else {
							output.column(columnName).insertObjectValue(
									output.column(columnName).getRows(),
									tempDataset.get(columnName), true);
						}
					}
				}
			}
		}

		return output;
	}
	public static void main(String[] arg) throws Exception {
		example example = new example();
		Data inputData = example.generate();
		String condition = "[{Age}+{Sex},Math.sqrt({Age}+{ID}),{Sex}+{Country}]";
		
		Data outputData = example.GenerateOutput(example.generateGroovyInput(inputData, condition), inputData);
		System.out.println("inputcondition: "+condition);
		for (int i = 0; i < outputData.getColumnCount(); i++) {
			
			
				System.out.println(outputData.column(i).getName() + ": "
						+ outputData.column(i).getList());
			
		}
	}
}
