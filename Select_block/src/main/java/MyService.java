
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;

public class MyService implements WorkflowBlock {
    /**
     * This field refers to property 'Copy Input' defined in service.xml
     */
    private final static String SELECT_CONDITION = "SELECT_CONDITION";

    /**
     * This field refers to input port 'input-1' defined in service.xml
     */
    private final static String Input_INPUT_1 = "input-1";
    
    /**
     * This field refers to output port 'output-1' defined in service.xml
     */
    private final static String Output_OUTPUT_1 = "output-1";


    /**
     * This method is called when block execution is first started. It should be
     * used to setup any data structures that are used throughout the execution
     * lifetime of the block.
     */
    public void preExecute(BlockEnvironment env) throws Exception
    {
        
    }

    /**
	 * This code is used to perform the actual block operation. It may be called
	 * multiple times if data is being streamed through the block. It is,
	 * however, guaranteed to be called at least once and always after the
	 * preExecute method and always before the postExecute method;
	 */
	public void execute(BlockEnvironment env, BlockInputs inputs,
			BlockOutputs outputs) throws Exception {
		 List<String> SelectedColumnName=new ArrayList<>();
		Data outputData = new Data();
		// Get the data from the input called "input-1"
		Data inputData = inputs.getInputDataSet(Input_INPUT_1);
		String condition = env.getStringProperty(SELECT_CONDITION, "");

		Pattern ColumnPattern = Pattern.compile("\\{.*?\\}");
		Matcher ColumnMatcher = ColumnPattern.matcher(condition);
		while (ColumnMatcher.find()) {
			String ColumnName = ColumnMatcher.group().replace("{", "")
					.replace("}", "");
			System.out.println(ColumnName);
			SelectedColumnName.add(ColumnName);
		}
		condition = condition.replace("{", "").replace("}", "");

		
		Binding context = new Binding();
		for (int row = 0; row < inputData.getLargestRows(); row++) {
			for (String column : SelectedColumnName) {
				context.setVariable(column, inputData.column(column)
						.getObjectValue(row));
				System.out.println(column + " :" + context.getVariable(column));
			}
			GroovyShell sh = new GroovyShell(context);
			System.out.println(condition);
			if (sh.evaluate(condition).getClass().equals(ArrayList.class)) {
				@SuppressWarnings("rawtypes")
				ArrayList tempDataset = (ArrayList) sh.evaluate(condition);
				for (int i = 0; i < tempDataset.size(); i++) {
					if (tempDataset.get(i).getClass().equals(String.class)) {
						System.out.println("This is string");

						if (!outputData.containsColumn("column" + i)) {
							StringColumn newStringcol = new StringColumn(
									"column" + i);
							newStringcol.insertObjectValue(
									newStringcol.getRows(), tempDataset.get(i),
									true);
							outputData.addColumn(newStringcol);
						} else {
							outputData.column(i).insertObjectValue(
									outputData.column(i).getRows(),
									tempDataset.get(i), true);
						}

					} else if (tempDataset.get(i).getClass().equals(Long.class)) {
						System.out.println("This is long");
						if (!outputData.containsColumn("column" + i)) {
							IntegerColumn newIntegercol = new IntegerColumn(
									"column" + i);
							newIntegercol.insertObjectValue(
									newIntegercol.getRows(),
									tempDataset.get(i), true);

							outputData.addColumn(newIntegercol);
						} else {
							outputData.column(i).insertObjectValue(
									outputData.column(i).getRows(),
									tempDataset.get(i), true);
						}
					} else if (tempDataset.get(i).getClass()
							.equals(Double.class)) {
						System.out.println("This is double");
						if (!outputData.containsColumn("column" + i)) {
							DoubleColumn newDoublecol = new DoubleColumn(
									"column" + i);
							newDoublecol.insertObjectValue(
									newDoublecol.getRows(), tempDataset.get(i),
									true);

							outputData.addColumn(newDoublecol);
						} else {
							outputData.column(i).insertObjectValue(
									outputData.column(i).getRows(),
									tempDataset.get(i), true);
						}
					}
				}
			} else if (sh.evaluate(condition).getClass()
					.equals(LinkedHashMap.class)) {
				@SuppressWarnings("rawtypes")
				LinkedHashMap tempDataset = (LinkedHashMap) sh
						.evaluate(condition);

				@SuppressWarnings("rawtypes")
				Set key = tempDataset.keySet();
				for (int i = 0; i < key.size(); i++) {

					String columnName = key.toArray()[i].toString();
					System.out.println("tempcol: " + columnName
							+ "  tempresult: " + tempDataset.get(columnName));
					if (tempDataset.get(columnName).getClass()
							.equals(String.class)) {
						System.out.println("This is string");
						if (!outputData.containsColumn(columnName)) {
							StringColumn newStringcol = new StringColumn(
									columnName);
							newStringcol.insertObjectValue(
									newStringcol.getRows(),
									tempDataset.get(columnName), true);
							outputData.addColumn(newStringcol);
						} else {
							outputData.column(columnName).insertObjectValue(
									outputData.column(columnName).getRows(),
									tempDataset.get(columnName), true);
						}

					} else if (tempDataset.get(columnName).getClass()
							.equals(Long.class)) {
						System.out.println("This is long");
						if (!outputData.containsColumn(columnName)) {
							IntegerColumn newIntegercol = new IntegerColumn(
									columnName);
							newIntegercol.insertObjectValue(
									newIntegercol.getRows(),
									tempDataset.get(columnName), true);

							outputData.addColumn(newIntegercol);
						} else {
							outputData.column(columnName).insertObjectValue(
									outputData.column(columnName).getRows(),
									tempDataset.get(columnName), true);
						}
					} else if (tempDataset.get(columnName).getClass()
							.equals(Double.class)) {
						System.out.println("This is double");
						if (!outputData.containsColumn(columnName)) {
							DoubleColumn newDoublecol = new DoubleColumn(
									columnName);
							newDoublecol.insertObjectValue(
									newDoublecol.getRows(),
									tempDataset.get(columnName), true);

							outputData.addColumn(newDoublecol);
						} else {
							outputData.column(columnName).insertObjectValue(
									outputData.column(columnName).getRows(),
									tempDataset.get(columnName), true);
						}
					}
				}
			}
		}
	
		// Pass this to the output called "output-1"
		outputs.setOutputDataSet(Output_OUTPUT_1, outputData);
	}
    
    /*
     * This code is called once when all of the data has passed through the block. 
     * It should be used to cleanup any resources that the block has made use of.
     */
    public void postExecute(BlockEnvironment env) throws Exception
    {
        
    }
}
