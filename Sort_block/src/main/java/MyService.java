
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.RowExtractor;

public class MyService implements WorkflowBlock {
	/**
	 * This field refers to property 'Copy Input' defined in service.xml
	 */
	private final static String Sort_condition = "Sort by";
	/**
	 * This field refers to input port 'input-1' defined in service.xml
	 */
	private final static String Input_INPUT_1 = "input-1";

	/**
	 * This field refers to output port 'output-1' defined in service.xml
	 */
	private final static String Output_OUTPUT_1 = "output-1";

	/**
	 * This method is called when block execution is first started. It should be
	 * used to setup any data structures that are used throughout the execution
	 * lifetime of the block.
	 */
	public void preExecute(BlockEnvironment env) throws Exception {

	}

	/**
	 * This code is used to perform the actual block operation. It may be called
	 * multiple times if data is being streamed through the block. It is,
	 * however, guaranteed to be called at least once and always after the
	 * preExecute method and always before the postExecute method;
	 */
	public void execute(BlockEnvironment env, BlockInputs inputs,
			BlockOutputs outputs) throws Exception {
		Data outputData = new Data();
		Data inputData = inputs.getInputDataSet(Input_INPUT_1);

		String[][] conditionList = env.getStringMatrixProperty(Sort_condition);
		List<Data> compareData = new ArrayList<>();
		RowExtractor rows = new RowExtractor(inputData);

		for (int i = 0; i < inputData.getLargestRows(); i++) {
			Data compareRow = rows.extract(i);
			compareData.add(compareRow);
		}
		Pattern ColumnPattern = Pattern.compile("\\{.*?\\}");

		for (int i = 0; i < conditionList.length; i++) {
			String condition = conditionList[i][0];
			Matcher ColumnMatcher = ColumnPattern.matcher(condition);
			final List<String> SelectedColumnName = new ArrayList<>();
			while (ColumnMatcher.find()) {
				String ColumnName = ColumnMatcher.group().replace("{", "")
						.replace("}", "");
				System.out.println(ColumnName);
				SelectedColumnName.add(ColumnName);
			}

			String SortDirection = "";

			if (!conditionList[i][1].isEmpty()) {
				SortDirection = conditionList[i][1];
			} else {
				SortDirection = "ASC";
			}
			final String Groovyinput = condition.replace("{", "").replace("}",
					"");
			System.out.println("Groovyinput: " + Groovyinput);
			System.out.println("SortDirection: " + SortDirection);
			if (SortDirection.equals("ASC")) {

				System.out.println("This is ASC sort Direction ");
				Collections.sort(compareData, new Comparator<Data>() {
					@Override
					public int compare(Data arg0, Data arg1) {
						Binding context_0 = new Binding();
						for (String columName : SelectedColumnName) {
							try {
								context_0.setVariable(columName,
										arg0.column(columName)
												.getObjectValue(0));
							} catch (IndexOutOfBoundsException | DataException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						GroovyShell sh_0 = new GroovyShell(context_0);
						Object x = sh_0.evaluate(Groovyinput);

						System.out.println("X: " + x);
						Binding context_1 = new Binding();
						for (String columName : SelectedColumnName) {
							try {
								context_1.setVariable(columName,
										arg1.column(columName)
												.getObjectValue(0));
							} catch (IndexOutOfBoundsException | DataException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						GroovyShell sh_1 = new GroovyShell(context_1);
						Object y = sh_1.evaluate(Groovyinput);
						System.out.println("Y: " + y);
						if (x instanceof BigDecimal && y instanceof BigDecimal) {
							BigDecimal value_1 = (BigDecimal) x;
							BigDecimal value_2 = (BigDecimal) y;
							System.out.println("BigDecimal: "
									+ value_1.compareTo(value_2));
							return value_1.compareTo(value_2);
						} else if (x instanceof Double && y instanceof Double) {
							System.out.println("Double: "
									+ Double.compare((Double) x, (Double) y));
							return Double.compare((Double) x, (Double) y);
						} else if (x instanceof Long && y instanceof Long) {
							System.out.println("Long: "
									+ Long.compare((Long) x, (Long) y));
							return Long.compare((Long) x, (Long) y);
						} else {
							System.out.println("String: "
									+ String.valueOf(x).compareTo(
											String.valueOf(y)));
							return String.valueOf(x).compareTo(
									String.valueOf(y));
						}
					}
				});
			} else if (SortDirection.equals("DESC")) {
				System.out.println("This is DESC sort Direction ");
				Collections.sort(compareData,
						Collections.reverseOrder(new Comparator<Data>() {
							@Override
							public int compare(Data o1, Data o2) {
								Binding context_0 = new Binding();
								for (String columName : SelectedColumnName) {
									try {
										context_0.setVariable(columName, o1
												.column(columName)
												.getObjectValue(0));
									} catch (IndexOutOfBoundsException
											| DataException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								GroovyShell sh_0 = new GroovyShell(context_0);
								Object x = sh_0.evaluate(Groovyinput);
								System.out.println("X: " + x);
								Binding context_1 = new Binding();
								for (String columName : SelectedColumnName) {
									try {
										context_1.setVariable(columName, o2
												.column(columName)
												.getObjectValue(0));
									} catch (IndexOutOfBoundsException
											| DataException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								GroovyShell sh_1 = new GroovyShell(context_1);

								Object y = sh_1.evaluate(Groovyinput);
								System.out.println("Y: " + y);

								if (x instanceof BigDecimal
										&& y instanceof BigDecimal) {
									BigDecimal value_1 = (BigDecimal) x;
									BigDecimal value_2 = (BigDecimal) y;
									System.out.println("BigDecimal: "
											+ value_1.compareTo(value_2));
									return value_1.compareTo(value_2);
								} else if (x instanceof Double
										&& y instanceof Double) {
									System.out.println("Double: "
											+ Double.compare((Double) x,
													(Double) y));
									return Double.compare((Double) x,
											(Double) y);
								} else if (x instanceof Long
										&& y instanceof Long) {
									System.out.println("Long: "
											+ Long.compare((Long) x, (Long) y));
									return Long.compare((Long) x, (Long) y);
								} else {
									System.out.println("String: "
											+ String.valueOf(x).compareTo(
													String.valueOf(y)));
									return String.valueOf(x).compareTo(
											String.valueOf(y));
								}
							}
						}));

			}
		}

		for (Data finalData : compareData) {
			if (outputData.getColumnCount() == 0) {
				outputData.addColumns(finalData.getColumnAray());
			} else {
				outputData.appendRows(finalData, true);
			}
		}
		outputs.setOutputDataSet(Output_OUTPUT_1, outputData);
	}
	public void postExecute(BlockEnvironment env) throws Exception {

	}
}
